const express = require('express');
const app = express();
const http = require('http');
const cors = require('cors');
const { Server } = require('socket.io');

app.use(cors());
const server = http.createServer(app);

const io = new Server(server, {
    cors: {
        origin: "http://localhost:19006",
        methods: ["GET", "POST"],
    },
});

io.on("connection", (socket) => {
    console.log('User Connected: ', socket.id);

    io.on("disconnect", () => {
        console.log('User Disconnected: ', socket.id);
    });
});


app.listen(4000, () => {
    console.log('Server is running...');
});